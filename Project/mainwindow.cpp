#include "mainwindow.h"
#include "qevent.h"
#include "ui_mainwindow.h"

BingoFileParser bingoFileParser;
bool MainWindow::sounds = true;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {

    ui->setupUi(this);
    allBingos = bingoFileParser.getAllFiles();
    QStringListModel* model = new QStringListModel(this);
    for(string vectorString : allBingos) {
        QString text = QString::fromStdString(vectorString);
        list << text;
    }
    model->setStringList(list);
    ui->listView_bingos->setModel(model);
    ui->listView_bingos->setCurrentIndex(model->index(0,0));
    chosenBingo = model->index(0,0).data(Qt::DisplayRole).toString();
    srand(time(0));
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::keyPressEvent( QKeyEvent* event) {
    switch(event->key()) {
        case Qt::Key_Control:
            ui->textBrowser_changelog->setText(
                        "QSysInfo\n" +
                        QSysInfo::kernelType() + " " + QSysInfo::currentCpuArchitecture() + "\n" +
                        QSysInfo::kernelVersion() + "\n" +
                        QSysInfo::productType() + " " + QSysInfo::productVersion() + "\n" +
                        QSysInfo::prettyProductName() + "\n" +
                        QSysInfo::machineHostName() + "\n" +
                        QSysInfo::machineUniqueId() + "\n" +
                        QSysInfo::bootUniqueId() + "\n"
                        );
        break;
    }
}

void MainWindow::on_listView_bingos_clicked(const QModelIndex &index) {
    chosenBingo = index.data(Qt::DisplayRole).toString();
}


void MainWindow::on_actionD_wi_ki_triggered(bool checked)
{
    sounds = checked;
}

void MainWindow::on_listView_bingos_doubleClicked(const QModelIndex &index) {
    chosenBingo = index.data(Qt::DisplayRole).toString();
    open_bingo_window(chosenBingo);
}

void MainWindow::on_pushButton_Launch_clicked() {
    open_bingo_window(chosenBingo);
}

void MainWindow::open_bingo_window(QString chosenBingo) {
    bingoDialog = new Bingo(this);
    bingoDialog->setBingoFilePath("bingos/" + chosenBingo.toStdString() + ".bgo");
    bingoDialog->onCreate();
    bingoDialog->setAttribute(Qt::WA_DeleteOnClose);
    bingoDialog->show();
}

void MainWindow::on_pushButton_Edit_clicked() {
    QString filePath = "/bingos/" + chosenBingo + ".bgo";
    QUrl url = QUrl("file://" + QCoreApplication::applicationDirPath() + filePath);
    QDesktopServices::openUrl(url);
    cout << url.toString().toStdString();
}

