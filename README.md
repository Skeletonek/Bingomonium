# Bingomonium
Qt C++ application to create, manage and play your own bingos

# License

This software is licensed under GNU GPL v3 license. You can read full version of this license here - [LICENSE](https://github.com/Skeletonek/Bingomonium/blob/main/LICENSE)

# Install
There are pre-builded releases available to download here - [Releases](https://github.com/Skeletonek/Bingomonium/releases)

### Windows
It should contain everything needed to run. Just launch the .exe

### Linux
Make sure you have Qt6-qtbase and Qt6-qtmultimedia package installed in your distribution. Afterwards, run executable.

# Compiling

### Qt Creator app
This is the easiest way to compile the project.  
Install the Qt Creator app for your system and install the Qt6 SDK package with Qt Multimedia support. You can also just install the qt6-base-dev and qt6-multimedia-dev packages if you are using a package manager.  
Clone the git repository and run the Bingomonium.pro file from Qt Creator app. You can compile the app with a button located in lower-left corner.

### Command line
Clone the git repository, create a build folder and in it run the qmake command pointing to the Bingomonium.pro file. After that run `make debug` or `make release` to compile application. On Linux systems the commands would look like this:

```
git clone git@gitlab.com:Skeletonek/Bingomonium.git
cd ./Bingomonium
mkdir build
cd ./build
```
Some Linux distributions provide the qmake for Qt6 via a qmake6 command so you need to use the Qt6 based one
```
qmake ../Project/Bingomonium.pro
```
or
```
qmake6 ../Project/Bingomonium.pro
```
Afterwards run the make command
```
make release
```
or
```
make debug
```
